﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneText : MonoBehaviour
{
    public Text sceneText;
    // Update is called once per frame
    void Start()
    {
        sceneText.text = SceneManager.GetActiveScene().name;
    }
}
