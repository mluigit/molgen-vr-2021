﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepressorController : MonoBehaviour {

    public bool active;
    public bool boundToOperator;
    public Molecule activeMolecule;
    public Molecule allolactose;
    private float inactiveTime;
    private float delay = 15f; //how long to wait until switching to active form

    private void Start()
    {
        inactiveTime = Time.time;
    }

    public void BindToOperator(bool bound)
    {
        boundToOperator = bound;
    }

    private void FixedUpdate()
    {
        //periodically release lactose
        if (active)
            return;

        if(Time.time - inactiveTime >= delay)
        {
            MoleculeManager.Instance.SpawnMolecule(activeMolecule, transform);
            MoleculeManager.Instance.SpawnMolecule(allolactose, transform);
            MoleculeStateMachine moleculeStateMachine = GetComponentInParent<MoleculeStateMachine>();
            Destroy(moleculeStateMachine.gameObject);

        }
    }
}
