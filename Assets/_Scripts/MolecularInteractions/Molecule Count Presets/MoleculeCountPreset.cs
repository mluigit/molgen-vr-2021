﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Molecule/Molecule Count Preset")]
public class MoleculeCountPreset : ScriptableObject {

    public int glucoseCount;
    public int lactoseCount;
}
