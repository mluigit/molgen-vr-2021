﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;
using UnityEngine.Playables;


[RequireComponent(typeof(PlayableDirector))]
public class PlayableDirectorStateMachine : StateMachine {


    [SerializeField]
    private PlayableDirector _director;

    private void Awake()
    {
        _director = GetComponent<PlayableDirector>();
    }


    //called by PlayableDirectorAction on each State
    public void Play(TimelineAsset timelineAsset, DirectorWrapMode wrapMode = DirectorWrapMode.None)
    {
        if (_director == null)
            return;

        //assign timeline asset to director
        _director.playableAsset = timelineAsset;

        //set wrap mode
        _director.extrapolationMode = wrapMode;

        //play the timeline
        _director.Play();
    }


    // --- UTILS --- //
    public PlayState CurrentPlayState()
    {
        return _director.state;
    }

    public double CurrentTime()
    {
        return _director.time;
    }

    public bool IsPlaying()
    {
        if (_director.state == PlayState.Playing)
            return true;
        return false;
    }

}
