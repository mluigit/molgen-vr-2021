﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName =("Molecule/Bind Actions/Set CAP"))]
public class SetCap : BindAction {

    public bool cap;

    public override void Act(MoleculeStateMachine moleculeStateMachine, MoleculeStateMachine boundMolecule, Transform activeSite)
    {
        if (MoleculeManager.Instance != null)
            MoleculeManager.Instance.Cap = cap;
    }
}
