﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Molecule/Bind Actions/Copy RNA")]
public class CopyRNA : BindAction {

    public override void Act(MoleculeStateMachine moleculeStateMachine, MoleculeStateMachine boundMolecule, Transform activeSite)
    {
        Molecule mRNAtype = boundMolecule.molecule;

        mRNAObjectPooler pooler = FindObjectOfType<mRNAObjectPooler>();

        pooler.SpawnmRNA(moleculeStateMachine, boundMolecule, mRNAtype);
    }
}
