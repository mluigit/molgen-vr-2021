﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Molecule/State Action/Call Spawn Actions")]
public class CallSpawnActions : StateAction {

    public override void Act(StateMachine stateMachine)
    {
        MoleculeStateMachine moleculeStateMachine = stateMachine as MoleculeStateMachine;

        if (moleculeStateMachine != null)
            moleculeStateMachine.SpawnActions();
    }
}
