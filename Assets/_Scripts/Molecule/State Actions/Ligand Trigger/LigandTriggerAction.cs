﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Molecule/Actions/Ligand Trigger")]
public class LigandTriggerAction : StateAction{

    public override void Act(StateMachine stateMachine)
    {
        MoleculeStateMachine moleculeStateMachine = stateMachine as MoleculeStateMachine;

        if (moleculeStateMachine == null)
            return;


        moleculeStateMachine.IsLigandInRange();
    }
}
