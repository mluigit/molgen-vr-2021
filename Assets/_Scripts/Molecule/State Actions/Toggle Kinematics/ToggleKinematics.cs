﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Molecule/Actions/Toggle Kinematics")]
public class ToggleKinematics : StateAction {

    public bool isKinematic;

    public override void Act(StateMachine stateMachine)
    {
        Rigidbody rigidbody = stateMachine.GetComponentInChildren<Rigidbody>();
        if (rigidbody != null)
            rigidbody.isKinematic = isKinematic;
    }
}
