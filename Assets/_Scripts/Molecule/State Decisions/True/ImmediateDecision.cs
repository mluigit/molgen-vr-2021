﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Molecule/Decisions/Immediate")]
public class ImmediateDecision : StateDecision {

    public override bool Decide(StateMachine stateMachine)
    {
        return true;
    }
}
