﻿//Sorry in advance - O.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Graph : MonoBehaviour
{
    public Transform pointPrefab;
    public Transform glucosePrefab;
    public Transform lactosePrefab;

    Transform[] points;
    [Range(10, 100)] public int resolution = 10;


    /// <summary>
    /// new variables based on WindowGraph
    /// </summary>
    public BacteriaGraph bGraph;

    public int total;
    public int lag = 10;
    public int maxCellDensity = 50;
    public int cellDensity;

    //For graph status
    public bool lagComplete;

    public bool drawingLag;
    public bool drawingGlucose;
    public bool drawingLactose;
    public bool drawingStationary;
    public bool drawingDeath;

    //For graph visibility
    public bool isGraphOn = true;
    public bool isCubeVisible = true;
    public MeshRenderer[] meshRenderers;



    //Renderer rend; //To change the material colour


    // Start is called before the first frame update
    void Start()
    {
        //rend = pointPrefab.GetComponent<Renderer>();

        StartCoroutine(CreateCubes());

    }

    // Update is called once per frame
    void Update()
    {

        //Debug.Log("GLUCOSE LEVEL: " + bGraph.getGlucose());
        //Debug.Log("LACTOSE LEVEL: " + bGraph.getLactose());
        ClearCubes();
        MoveGraph();

        if (Input.GetKeyDown(KeyCode.P))
        {
            isGraphOn = !isGraphOn;
            ToggleGraph();
        }
        

    }

    private void Awake()
    {
        /*float step = 2f / resolution;
        Vector3 scale = Vector3.one * step;
        Vector3 position;
        position.y = 0f;
        position.z = 0f;

        points = new Transform[resolution];
        */

    }

    Vector3 currentY;
    Vector3 currentX;
    List<float> cList = new List<float>();
    float[] cArray = new float[5]; //index 0=lag, 1=glucose, 2=lactose, 3=stationary, 4=death
    int lastDrawn; //Keeps track of the last drawn slope, value corresponds to an index of cArray
    IEnumerator CreateCubes()
    {
        float step = 2f / resolution;
        Vector3 position;
        position.z = 0f;

        //While there is glucose in the system
        for (int i = 0; i < 50000; i++)
        {
            yield return new WaitForSeconds(1); //Wait x seconds before executing

            pointPrefab.GetComponent<MeshRenderer>().enabled = isCubeVisible; //Determine whether cubes should be rendered visible or not
            //Instantiate cubes
            Transform point = Instantiate(pointPrefab);
            pointPrefab.tag = "node"; //Set the tags for each cube to "node"
            

            //Rising glucose phase
            if (bGraph.getGlucose() > 0 && cellDensity!=maxCellDensity && lagComplete == true)
            {
                pointPrefab = glucosePrefab;

                //pointPrefab.GetComponent<Renderer>().material.color = Color.red;
                //pointPrefab.GetComponent<MeshRenderer>().material.SetColor("_Color", Color.red);

                if (lastDrawn != 0 && lastDrawn != 1)
                {
                    lagComplete = false;
                    lag = 10;
                }

                drawingGlucose = true;
                drawingLactose = false;
                drawingLag = false;
                drawingStationary = false;
                drawingDeath = false;

                Debug.Log("DRAWING GLUCOSE");
                position.y = currentY.y;
                position.x = (i + 0.5f) / 5f;
                position.y = (position.x - currentX.x) + cArray[0];
                point.localPosition = position;
                point.localScale = Vector3.one / 5f;
                point.SetParent(transform, false);
                UpdateCellDensity(1);
                currentY.y = position.y;

                if (drawingGlucose)
                {
                    lastDrawn = 1;
                    cArray[1] = position.y;    
                }

            }

           // Rising lactose phase
            else if (bGraph.getGlucose() == 0 && bGraph.getLactose() > 0 && lagComplete == true && cellDensity!=maxCellDensity)
            {
                pointPrefab = lactosePrefab;

                if(lastDrawn != 0 && lastDrawn != 2)
                {
                    lagComplete = false;
                    lag = 10;
                }
                
                drawingGlucose = false;
                drawingLactose = true;
                drawingLag = false;
                drawingStationary = false;
                drawingDeath = false;

                Debug.Log("DRAWING LACTOSE");
                //rend.material.SetColor("_color", Color.blue);
                Debug.Log("CURRENT X: " + currentX.x);
                position.y = currentY.y;
                position.x = (i + 0.5f) / 5f;
                position.y =  (0.577f * (position.x - currentX.x) + cArray[0]);
                //Debug.Log(position.y);
                point.localPosition = position;
                point.localScale = Vector3.one / 5f;
                point.SetParent(transform, false);
                //currentY.y = position.y;
                UpdateCellDensity(1);

                if (drawingLactose)
                {
                    lastDrawn = 2;
                    cArray[2] = position.y;   
                }
            }


            //Death phase
            else if (bGraph.getGlucose() == 0 && bGraph.getLactose() == 0 && lagComplete == true && cellDensity > 0)
            {
                if (lastDrawn != 0 && lastDrawn != 4)
                {
                    lagComplete = false;
                    lag = 10;
                }

                drawingGlucose = false;
                drawingLactose = false;
                drawingLag = false;
                drawingStationary = false;
                drawingDeath = true;

                position.y = currentY.y;
                position.x = (i + 0.5f) / 5f;
                position.y = -(position.x - currentX.x) + cArray[0];
                point.localPosition = position;
                point.localScale = Vector3.one / 5f;
                point.SetParent(transform, false);
                Debug.Log("DRAWING DEATH PHASE");
                UpdateCellDensity(-1); //Reduce MaxDensity

                if (drawingDeath)
                {
                    lastDrawn = 4;
                    cArray[4] = position.y;
                    
                }

             
            }

            //Stationary phase
            else if ((cellDensity == maxCellDensity || cellDensity == 0) && lagComplete == true)
            {
                if (lastDrawn != 0 && lastDrawn !=3)
                {
                    lagComplete = false;
                    lag = 10;
                }

                drawingGlucose = false;
                drawingLactose = false;
                drawingLag = false;
                drawingStationary = true;
                drawingDeath = false;


                Debug.Log("DRAWING STATIONARY");
                position.x = (i + 0.5f) / 5f;
                position.y = cArray[lastDrawn];
                point.localPosition = position; //Vector3.right * i / 5f;
                point.localScale = Vector3.one / 5f;
                point.SetParent(transform, false);

                currentX.x = position.x;
                currentY.y = position.y;

                if (drawingStationary)
                {
                    lastDrawn = 3;
                    cArray[3] = position.y; //Should be added when a phase ends
                    
                }
            }

            //Lag phase
            else //if (bGraph.getGlucose() > 0 && bGraph.getLactose() > 0 && lag > 0)
            {
                //Could add a condition to check if its a lag period or a plateau

                //if (lastDrawn != 0 && lastDrawn != 3)
                //{
                //    lagComplete = false;
                //    lag = 10;
                //}
                

                drawingGlucose = false;
                drawingLactose = false;
                drawingLag = true;
                drawingStationary = false;
                drawingDeath = false;


                Debug.Log("DRAWING LAG");
                position.x = (i + 0.5f) / 5f;
                position.y = cArray[lastDrawn];
                point.localPosition = position; //Vector3.right * i / 5f;
                point.localScale = Vector3.one / 5f;
                point.SetParent(transform, false);

                lag--;

                if (lag < 0)
                {
                    lag = 0; //Prevent lag from going negative
                    lagComplete = true;
                }

                

                currentX.x = position.x;
                currentY.y = position.y;

                if (drawingLag)
                {
                    lastDrawn = 0;  
                    cArray[0] = currentY.y; //Should be added when a phase ends
                    
                }
               
               
            }

        }

    }

    //Change the cell density
    public void UpdateCellDensity(int amount)
    {
        cellDensity += amount;
        if (cellDensity < 0)
        {
            cellDensity = 0;
        }
    }

    void ToggleGraph()
    {
            meshRenderers = GetComponentsInChildren<MeshRenderer>();

            foreach (MeshRenderer meshRenderer in meshRenderers)
                meshRenderer.enabled = isGraphOn;

            isCubeVisible = isGraphOn;

    }


    //Destroy unneeded cubes
    void ClearCubes()
    {
        GameObject[] cubes = GameObject.FindGameObjectsWithTag("node"); //Add all the cubes to a list

        if (cubes.Length > 30)
        {
            Destroy(cubes[0]);
        }
        
        /*for (var i = 0; i < cubes.Length; i++)
        {
            Destroy(cubes[i]);
        }*/
        //checkTotal = total;
    }

    void MoveGraph()
    {
        GameObject graph = GameObject.FindGameObjectWithTag("cubeGraph");
        graph.transform.Translate(-0.08f* Time.deltaTime, 0, 0);
        // graph.transform.position.x - 1;
    }

    public void AddTotal(int add, int time)
    {
        total += add;
        if (total < 0)
        {
            total = 0;
        }

    }

    public int GetTotal()
    {
        return total;
    }


}
