﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlucoseGraphInformer : MonoBehaviour {

    private GameObject graph;
    // Use this for initialization
    void Start()
    {
        graph = GameObject.FindGameObjectWithTag("graph");
        graph.GetComponent<BacteriaGraph>().AddGlucose(+1);
    }

    private void OnDestroy()
    {
        graph.GetComponent<BacteriaGraph>().AddGlucose(-1);
    }
}
