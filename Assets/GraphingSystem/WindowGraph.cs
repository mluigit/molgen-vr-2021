﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WindowGraph : MonoBehaviour
{
    private List<int> valueList = new List<int>();
    private List<Color> colorList = new List<Color>();

    public List<List<int>> valueData = new List<List<int>>();
    public List<List<Color>> colorData = new List<List<Color>>();

    private int checkTotal;
    public int total;
    private float yMaximum = 50f;
    
    private RectTransform graphContainer;
    [SerializeField] private Sprite circleSprite;
    [SerializeField] private BacteriaGraph bGraph;

    private void Awake()
    {
        graphContainer = transform.Find("GraphContainer").GetComponent<RectTransform>();
        total = 0;
        checkTotal = total;
        valueList.Add(total);
        colorList.Add(new Color(255, 255, 255, 0f));
        ShowGraph(valueList, colorList);

        
    }


    private GameObject CreateCircle(Vector2 anchoredPosition)
    {
        GameObject gameObject = new GameObject("circle", typeof(Image));
        gameObject.transform.SetParent(graphContainer, false);
        gameObject.GetComponent<Image>().sprite = circleSprite;
        //change the color of sprite
        Color c = gameObject.GetComponent<Image>().color;
        c.a = 0;
        gameObject.GetComponent<Image>().color = c;
        RectTransform rectTransform = gameObject.GetComponent<RectTransform>();
        rectTransform.anchoredPosition = anchoredPosition;
        rectTransform.sizeDelta = new Vector2(11, 11);
        rectTransform.anchorMin = new Vector2(0, 0);
        rectTransform.anchorMax = new Vector2(0, 0);

        return gameObject;
    }

    private void MakeGridLines(float xSize)
    {
        float graphHeight = graphContainer.sizeDelta.y;
        float graphWidth = graphContainer.sizeDelta.x;
        float yMax = 50;

        //make vertical lines 
        for (int i = 0; i < graphWidth; i ++)
        {
            if (i*xSize > graphWidth)
            {
                break;
            }
            if (i % 10 == 0)
            {
                GameObject lastCircleGameObject = CreateCircle(new Vector2(i * xSize, 0));
                GameObject circleGameObject = CreateCircle(new Vector2(i * xSize, graphHeight));
                circleGameObject.tag = "node";
                lastCircleGameObject.tag = "node";
                CreateDotConnection(lastCircleGameObject.GetComponent<RectTransform>().anchoredPosition, circleGameObject.GetComponent<RectTransform>().anchoredPosition, new Color(0, 0, 0, 1f));

                GameObject text = new GameObject("number");
                text.transform.SetParent(lastCircleGameObject.transform, false);
                RectTransform rectTransform = text.AddComponent<RectTransform>();
                rectTransform.anchoredPosition = new Vector2(5f, 0);
                rectTransform.sizeDelta = new Vector2(18, 17);
                rectTransform.anchorMin = new Vector2(0, 0);
                rectTransform.anchorMax = new Vector2(0, 0);
                Text number = text.AddComponent<Text>();
                number.color = new Color(255, 255, 255);
                number.font = (Font)Resources.GetBuiltinResource(typeof(Font), "Arial.ttf");
                number.alignment = TextAnchor.MiddleCenter;
                number.fontSize = 10;

                if (valueData.Count == 0)
                {
                    number.text = (i).ToString();
                }
                else
                {
                    number.text = (i + 20 * (valueData.Count)).ToString();
                }
            }
            //if (i == 0)
            //{
            //    number.text = (20 * (1 + valueData.Count)).ToString();
            //}
            //else
            //{
            //    number.text = (i + 20 *(1+valueData.Count)).ToString();
            //}
        }

        //make horizontal lines
        for (int i = 0; i < graphHeight; i += 5)
        {
            float yPos = (i / yMax) * graphHeight;
            if (yPos > graphHeight)
            {
                break;
            }

            GameObject lastCircleGameObject = CreateCircle(new Vector2(0, yPos)); ;
            GameObject circleGameObject = CreateCircle(new Vector2(graphWidth, yPos));
            circleGameObject.tag = "node";
            lastCircleGameObject.tag = "node";
            CreateDotConnection(lastCircleGameObject.GetComponent<RectTransform>().anchoredPosition, circleGameObject.GetComponent<RectTransform>().anchoredPosition, new Color(0, 0, 0, 1f));
        }

    }


    private void ShowGraph(List<int> valueList, List<Color> colorList)
    {
        float graphHeight = graphContainer.sizeDelta.y;
        float graphWidth = graphContainer.sizeDelta.x;
        

        //Alt1: resize graph based on number of nodes
        //float xSize = graphContainer.sizeDelta.x / valueList.Count;

        //Alt2: only show x number of nodes at a time.
        float xSize = graphContainer.sizeDelta.x /60;

        MakeGridLines(xSize);
        if (valueList.Count >= 62)
        {
            colorData.Add(colorList);
            valueData.Add(valueList);

            valueList.Clear();
            colorList.Clear();
        }

        GameObject lastCircleGameObject = null;
        for (int i = 0; i < valueList.Count; i++)
        {
            float xPosition = (i * xSize);// * timeList[i]);
            float yPosition = (valueList[i] / yMaximum) * graphHeight;
            if (yPosition > graphHeight)
            {
                valueList[i] = (int)yMaximum;
                yPosition = graphHeight;
            }
            GameObject circleGameObject = CreateCircle(new Vector2(xPosition, yPosition));
            circleGameObject.tag = "node";

            if(lastCircleGameObject != null)
            {
                CreateDotConnection(lastCircleGameObject.GetComponent<RectTransform>().anchoredPosition, circleGameObject.GetComponent<RectTransform>().anchoredPosition, colorList[i]);
            }

            lastCircleGameObject = circleGameObject;
        }
    }

    private void CreateDotConnection(Vector2 dotPositionA, Vector2 dotPositionB, Color color)
    {
        GameObject gameObject = new GameObject("dotConnection", typeof(Image));
        gameObject.tag = "node";
        gameObject.transform.SetParent(graphContainer, false);        
        gameObject.GetComponent<Image>().color = color;
        RectTransform rectTransform = gameObject.GetComponent<RectTransform>();
        Vector2 dir = (dotPositionB - dotPositionA).normalized;
        float distance = Vector2.Distance(dotPositionA, dotPositionB);
        rectTransform.anchorMin = new Vector2(0, 0);
        rectTransform.anchorMax = new Vector2(0, 0);
        rectTransform.sizeDelta = new Vector2(distance, 3f);
        rectTransform.anchoredPosition = dotPositionA + dir * distance *0.5f;
        //TODO: Complete line joining
        Vector2 tempPoint = new Vector2(dotPositionA.x, dotPositionB.y);

        rectTransform.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(dotPositionB.y - dotPositionA.y,(dotPositionB.x - dotPositionA.x))* 180f/ Mathf.PI);
    }

    public void AddTotal(int add, int time)
    {
        total += add;
        if (total < 0)
        {
            total = 0;
        }

        //timeList.Add(time);
        ResetGraph(time);
    }

    public void ResetGraph(int times)
    {
        for (int i = 0; i < times; i++)
        {
            valueList.Add(total);

            //Assign the color of the graph associated with the glucose or lactose level
            if (bGraph.getGlucose() == 0 && bGraph.getLactose() >= 1)
            {
                colorList.Add(new Color(0, 0, 255, 1f));
            }
            else
            {
                colorList.Add(new Color(255, 0, 0, 1f));
            }
        }
            GameObject[] gameObjects = GameObject.FindGameObjectsWithTag("node");
            for (var i = 0; i < gameObjects.Length; i++)
            {
                Destroy(gameObjects[i]);
            }
            ShowGraph(valueList, colorList);
            checkTotal = total;
     }

    public int GetTotal()
    {
        return total;
    }

    public void ShowPastGraph(int index)
    {
        ShowGraph(valueData[index], colorData[index]);
    }
}
